<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'judy');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NVh}n-.b0spyVg<i|0]B-4izsRTo+mb.h5%>]HF+FoV<D;;%?]N_F1|EZ-6e-c(R');
define('SECURE_AUTH_KEY',  '_2{d;QO]+/.n3nv)fsxcx04<7npx^T3]:>-Bh+,cyk+N1EsHc] FEIs#n<7j6}|n');
define('LOGGED_IN_KEY',    '/1!_OKVP Z/0-[A@j-in(eVZy5b(l&dOYJp8Ylj+|^(KFXj|CZyX#P0lv)ocxFM2');
define('NONCE_KEY',        '9@+[z6^GI`VxeQEv9+QWh=ega&~!:6bF?daZClrT)J[re(]mq8j+.O#u:&EueT/d');
define('AUTH_SALT',        '{OE_s:n3O43BKepV0mZwZ$@*6oILdWQG*C48Qf`c@$HvU`!]I7n{27Mx|JBTGoOe');
define('SECURE_AUTH_SALT', 'y{Lty<7R~j[Q 1A6@+szYtq-<D/+x9`!v{j~[M:kO+JI^J4W%|-+0;|qT]o&Mm4l');
define('LOGGED_IN_SALT',   '}C]>X4VT]y%u]4K+ZaO23(+</S$jgE/eZ&S!|Pbq+9z7hY!GmU-2NKU{OP+||W58');
define('NONCE_SALT',       'S=zQjCXkN@95~-Jb=$wTl;}+,+2^sO-3diaq#D}!3%M-*b6ZEG8GDH}TH1C8D1R#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
